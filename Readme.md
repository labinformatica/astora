<img src="render_v2/astora_v2.jpg" alt="Astora V2"/>


# Robot Astora

[[_TOC_]]

## Introducción

Astora es un robot terrestre con aplicaciones didácticas cuya construcción se basa en el uso de Impresión 3D por deposición de material (FDM). Todo el diseño se ha desarrollo utilizando software libre y, en particular, FreeCAD. 

El foco del proyecto consiste en el diseño de la estructura, no así la electrónica asociada al mismo. En este sentido la implementación se ha realizado utilizando diferentes shields de Arduino así como una placa de desarrollo Arduino UNO como sistema de control. 

El sistema de dirección del robot se basa en un micro servo SG90 mientras que el sistema de propulsión utilizado es un motor con caja reductora controlado por un driver L298D. 

![driver][1] ![motor][2] ![servo][3]

[1]:img/driver_l298d.jpg
[2]:img/motor.jpg
[3]:img/servoSg90.jpg
"Shields utilizados por los dos modelos"

## Partes en común de ambas versiones

En el repositorio encontrará partes específicas para armar cada una de los dos versiones del robot, muchas de las partes son intercambiables o comunes a las dos versiones. La diferencia fundamental entre cada versión del robot está dada por la fuente de alimentación que utiliza, lo cuál repercute en el diseño del chasis superior e inferior. 

Una de las versiones (Astora V1) utiliza dos baterías de 9 [V] recargables, mientras que la versión 2 utiliza un pack de baterías 18650 a fin de dotar de energía al sistema. 

## Robot Astora V1
En esta versión el chasis incorpora dos soporte para baterías de 9 volt recargables, como una alternativa mas accesible, para la fuente de energía.

![Bateria Recargable 9 Volt][4]

*Baterias de 9 Volt que pueden ir en el robot.*

### Ensamble de la versión 1 del chasis (9V).
![Ensamble del chasis V1][5]

*Ensamble del robot. ![Ver modelo...][6]*


[4]:img/pilas-recargables-9v.jpg 
[5]:img/Astora_v1_Ensamble.png 
[6]:Ensambles/Ensamble_Version_A.stl
## Robot Astora V2

En esta versión se utilizó un controlador de carga y descarga para baterías de Litio Ion, el modelo utilizado es fabricado por DM [Link del producto](https://www.diymore.cc/collections/hot-sale/products/18650-battery-shield-v8-mobile-power-bank-3v-5v-for-arduino-esp32-esp8266-wifi "Controlador de carga y descarga"). Como ventaja, este módulo permite cargar las batería dentro del robot, a través de un cable micro usb. Además incorpora reguladores de 3.3 [V] y 5 [V].

![Controlador 18650](img/moduloBaterias.jpg)

### Ensamble de la versión 2 del chasis (18650).

![Ensamble del chasis V2][8]



*Ensamble del robot. ![Ver modelo...][7]*

[7]:Ensambles/Ensamble_Version_B.stl
[8]:img/Astora_V2_Ensamble.png 